/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package mx.hash.encriptado;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bouncycastle.openpgp.PGPException;

/**
 *
 * @author david
 */
public class App {
    static private final Logger LOGGER = Logger.getLogger("mx.hash.encriptado.App");

    public static void main(String[] args) {
        FileInputStream streamLlavePublica = null;
        try {
            File llavePublica = new File("llave.gpg");
            streamLlavePublica = new FileInputStream(llavePublica);
            
            EncriptadorDAO encriptadorDAO = new EncriptadorDAO();
            
            encriptadorDAO.inicializarBouncyCastle();
            
            String texto = "HOLA-MUNDO";
            
            byte[] datos = texto.getBytes();
            ByteArrayInputStream datosStream = new ByteArrayInputStream(datos);
            Integer longitudDatos = datos.length;            
            
            ByteArrayOutputStream salidaStream = new ByteArrayOutputStream();
            
            encriptadorDAO.encriptar(salidaStream, datosStream, longitudDatos, streamLlavePublica);
            
            byte[] datosEncriptados = salidaStream.toByteArray();
            
            String resultado = new String(datosEncriptados);
            
            LOGGER.log(Level.INFO, resultado);
            
            streamLlavePublica.close();
            
        } catch (FileNotFoundException ex) {
            LOGGER.log(Level.SEVERE, "Archivo llave no encontrado");
            ex.printStackTrace();
            
        } catch (IOException | PGPException ex) {
            LOGGER.log(Level.SEVERE, "Error de entrada/salida");
            ex.printStackTrace();
        } 
        
    }
}
