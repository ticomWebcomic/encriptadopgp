/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mx.hash.encriptado;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.security.Security;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.CompressionAlgorithmTags;
import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPLiteralDataGenerator;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;
import org.bouncycastle.openpgp.operator.jcajce.JcePGPDataEncryptorBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePublicKeyKeyEncryptionMethodGenerator;

/**
 *
 * @author david
 */
public class EncriptadorDAO {
    static private final Logger LOGGER = Logger.getLogger("mx.hash.encriptado.EncriptadorDAO");

    private final int algoritmoCompresion = CompressionAlgorithmTags.ZIP;
    private final int algoritmoLlaveSimetrica = SymmetricKeyAlgorithmTags.AES_128;
    private final boolean armadura = true;
    private final boolean chequeoIntegridad = true;
    private final int bufferSize = 1 << 16;

    public void inicializarBouncyCastle() {
        LOGGER.log(Level.INFO, "Inicializando Bouncy Castle");
        
        Security.addProvider(new BouncyCastleProvider());
    }

    public void encriptar(OutputStream encriptado, InputStream datosStream, long longitudDatos, InputStream llavePublicaStream) throws IOException, PGPException {
        LOGGER.log(Level.INFO, "Encriptando datos");
        
        // Compresor de datos
        PGPCompressedDataGenerator compresor = new PGPCompressedDataGenerator(algoritmoCompresion);

        // Objeto con las configuraciones para encriptacion
        JcePGPDataEncryptorBuilder constructorPGP = new JcePGPDataEncryptorBuilder(algoritmoLlaveSimetrica);
        constructorPGP.setWithIntegrityPacket(chequeoIntegridad);
        constructorPGP.setSecureRandom(new SecureRandom());
        constructorPGP.setProvider(BouncyCastleProvider.PROVIDER_NAME);

        // Encriptador de datos
        PGPEncryptedDataGenerator pgpEncryptedDataGenerator = new PGPEncryptedDataGenerator(constructorPGP);

        // Objeto de la llave publica
        PGPPublicKey llavePublica = obtenerLlavePublica(llavePublicaStream);
        
        // Metodo de encriptado 
        JcePublicKeyKeyEncryptionMethodGenerator metodo = new JcePublicKeyKeyEncryptionMethodGenerator(llavePublica);
        
        // Agregamos el metodo de encriptado deseado al encriptador
        pgpEncryptedDataGenerator.addMethod(metodo);

        // Si se esta usando la armadura generamos el Stream adecuado
        if (armadura) {
            encriptado = new ArmoredOutputStream(encriptado);
        }

        // Creamos el Stream con los datos encriptados
        OutputStream cipherOutStream = pgpEncryptedDataGenerator.open(encriptado, new byte[bufferSize]);

        // Generamos datos finales apartir del Stream de datos encriptados
        generarDatos(compresor.open(cipherOutStream), datosStream, longitudDatos, bufferSize);
        
        // Cerramos los streams
        compresor.close();
        cipherOutStream.close();
        encriptado.close();

    }
    
    public PGPPublicKey obtenerLlavePublica(InputStream keyInputStream) throws IOException, PGPException {
        LOGGER.log(Level.INFO, "Obteniendo llave Publica");
        
        PGPPublicKeyRingCollection pgpPublicKeyRings = new PGPPublicKeyRingCollection( PGPUtil.getDecoderStream(keyInputStream), new JcaKeyFingerprintCalculator());
        
        Iterator<PGPPublicKeyRing> keyRingIterator = pgpPublicKeyRings.getKeyRings();
        
        while (keyRingIterator.hasNext()) {
            PGPPublicKeyRing pgpPublicKeyRing = keyRingIterator.next();
            Optional<PGPPublicKey> pgpPublicKey = extraerPGPKey(pgpPublicKeyRing);
            
            if (pgpPublicKey.isPresent()) {
                return pgpPublicKey.get();
            }
        }
        
        throw new PGPException("Llave publica invalida");
    }

    public Optional<PGPPublicKey> extraerPGPKey(PGPPublicKeyRing pgpPublicKeyRing) {
        LOGGER.log(Level.INFO, "Extrayendo llave PGP");
        
        for (PGPPublicKey publicKey : pgpPublicKeyRing) {
            if (publicKey.isEncryptionKey()) {
                return Optional.of(publicKey);
            }
        }
        return Optional.empty();
    }

    public void generarDatos(OutputStream copia, InputStream datosOriginales, long longitudDatos, int bufferSize) throws IOException {
        LOGGER.log(Level.INFO, "Generando datos finales");
        
        PGPLiteralDataGenerator literalDataGenerator = new PGPLiteralDataGenerator();        
        
        OutputStream pOut = literalDataGenerator.open(copia, PGPLiteralData.BINARY, PGPLiteralData.CONSOLE, Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC)), new byte[bufferSize]);
        
        byte[] buffer = new byte[bufferSize];
        
        try {
            int len;
            long totalBytesWritten = 0L;
            
            while (totalBytesWritten <= longitudDatos && (len = datosOriginales.read(buffer)) > 0) {
                pOut.write(buffer, 0, len);
                totalBytesWritten += len;
            }
            
            pOut.close();
        } finally {
            // Clearing buffer
            Arrays.fill(buffer, (byte) 0);
            // Closing inputstream
            datosOriginales.close();
        }
    }
}
